﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApplication2.Controllers
{
    public class PersonaController : ApiController
    {
        [HttpPost]
        public IHttpActionResult Add(Models.Request.Personarequest modelo)
        {
            using (Models.Pruebas_DarioEntities db = new Models.Pruebas_DarioEntities())
            {
                var oPersona = new Models.Persona();
                oPersona.nombre = modelo.Nombre;
                oPersona.edad = modelo.Edad;
                db.Persona.Add(oPersona);
                db.SaveChanges();


            }

            return Ok("Ok");
                    }


        [HttpGet]
        public IHttpActionResult Get()
        {
            List<ViewModels.PersonaViewModel> lst = new List<ViewModels.PersonaViewModel>();
            using (Models.Pruebas_DarioEntities db = new Models.Pruebas_DarioEntities())
            {
                lst = (from d in db.Persona
                       select new ViewModels.PersonaViewModel
                       {
                           Id = d.id,
                           Nombre = d.nombre,
                           Edad = d.edad
                       }).ToList();
                
            }

            return Ok(lst);


        }

    }
}
