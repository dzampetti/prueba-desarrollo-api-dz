﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication2.Gestores;
using WebApplication2.Models;


namespace WebApplication2.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/mutuales")]
    public class MutualController : ApiController
    {
        [HttpGet]
        [Route("ConsultarMutuales")]
        public IHttpActionResult ConsultarMutuales()
        {
            try
            {
                List<Mutual> lista = new List<Mutual>();
                lista = GestorMutuales.getInstance().ConsultarMutuales();

                return Ok(lista);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.InternalServerError, "Error indefinido en la API");
            }
        }

        [HttpGet]
        [Route("ConsultarMutuales_Opcion2")]
        public IHttpActionResult ConsultarMutuales_Opcion2()
        {
            List<ViewModels.MutualViewModel> lst = new List<ViewModels.MutualViewModel>();
            using (Models.Pruebas_DarioEntities db = new Models.Pruebas_DarioEntities())
            {
                lst = (from d in db.Mutuales
                       select new ViewModels.MutualViewModel
                       {
                           Id = d.Id,
                           Descripcion = d.Descripcion
                         
                       }).ToList();

            }

            return Ok(lst);


        }

       
        [HttpGet]
        [Route("ConsultarMutual")]
        public IHttpActionResult ConsultarMutual(int id)
        {
           
            try
            {
             
                
                return Ok();
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.InternalServerError, "Error indefinido en la API");
            }
        }


        [HttpGet]
        [Route("ConsultarMutual_Opcion2")]
        public IHttpActionResult ConsultarMutual_Opcion2(int id)
        {
            //if (id.Equals(Guid.Empty))
            //{
            //    return BadRequest("El parametro es necesario");
            //}

            List<ViewModels.MutualViewModel> lst = new List<ViewModels.MutualViewModel>();
            using (Models.Pruebas_DarioEntities db = new Models.Pruebas_DarioEntities())
            {
                lst = (from d in db.Mutuales.Where(s => s.Id == id)
                       select new ViewModels.MutualViewModel
                       {
                           Id = d.Id,
                           Descripcion = d.Descripcion

                       }).ToList();

            }

            



            return Ok(lst);


        }


        [HttpGet]
        [Route("ConsultarMutualesPorDescripcion")]
        public IHttpActionResult ConsultarMutualesPorDescripcion(string busqueda)
        {
            /*Este método es el que se consumiria en caso de que se quisiera consultar mutuales por coincidencia en la descripción
             de las mismas*/
            //Deben desarrollar la lógica del mismo con las clases provistas en el proyecto



            //En caso de camino exitoso deben devolver el resultado utilizando el metodo Ok
            //return Ok(lo que vayan a devolver)

            /*En caso de que se detecten algún problema se va a utilizar el método Content
              return return Content(Http status code, mensaje informando el problema);
              Ej: return Content(HttpStatusCode.InternalServerError, "Error al consultar mutuales");
              Si no conocen los http status code, investiguen un poco lo que son, para que puedan decidir
              que código es más adecuado informar según la situación*/

            return null; //cuando programen el método borren esta linea
        }
    }
}
