﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication2.Models;

namespace WebApplication2.Gestores
{
    public class GestorMutuales
    {
        #region "PATRON SINGLETON"
        private static GestorMutuales mutual = null;
        private GestorMutuales() { }
        public static GestorMutuales getInstance()
        {
            if (mutual == null)
            {
                mutual = new GestorMutuales();
            }
            return mutual;
        }
        #endregion

        // Aquí se debe crear un método que devuelva el listado de mutuales
        public List<Mutual> ConsultarMutuales()
        {
            try
            {
                List<Mutual> m = new List<Mutual>();
                m = Mutuales();
                return m;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        // Aquí se debe crea un método que devuelva una mutual consultada por su N° Id
        public Mutuales ConsultarMutual(int Id)
        {
            Mutuales MutualResultante = new Models.Mutuales();
            try
            {
                //using (Models.Pruebas_DarioEntities db = new Models.Pruebas_DarioEntities())
                using (var db = new Pruebas_DarioEntities())
                {
                    MutualResultante = db.Mutuales.Where(a => a.Id == Id).FirstOrDefault();
                }    
                                 
                return MutualResultante;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        // Aquí se debe crea un método que devuelva un listado de mutuales cuya descripción   contenga un texto enviado por parámetro
        /*
         * 
         * 
         * 
         * 
         * 
         * /

         /* Al no tener un servidor de desarrollo con una base de datos donde transaccionar,
            se utiliza este método a modo de simulacion de una tabla Mutual*/
        private List<Mutual> Mutuales()
        {
            List<Mutual> mutuales = new List<Mutual>();
            Mutual m = new Mutual();

            mutuales.Add(new Mutual() { Id = 1041, Descripcion = "OSDE" });
            mutuales.Add(new Mutual() { Id = 2980, Descripcion = "TEXTILES" });
            mutuales.Add(new Mutual() { Id = 4625, Descripcion = "OMINT" });
            mutuales.Add(new Mutual() { Id = 4723, Descripcion = "SWISS MEDICAL" });
            mutuales.Add(new Mutual() { Id = 4730, Descripcion = "JERARQUICOS SALUD" });
            mutuales.Add(new Mutual() { Id = 4751, Descripcion = "LUZ Y FUERZA" });
            mutuales.Add(new Mutual() { Id = 4940, Descripcion = "GALENO" });
            mutuales.Add(new Mutual() { Id = 4975, Descripcion = "MEDIFE" });
            mutuales.Add(new Mutual() { Id = 5073, Descripcion = "PAMI" });

            return mutuales;
        }
    }
}