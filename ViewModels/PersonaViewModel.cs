﻿using System;

namespace ViewModels
{
    public class PersonaViewModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int? Edad { get; set; }
    }
}
